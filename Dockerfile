FROM registry.simplicite.io/platform:5-latest
COPY modules /usr/local/tomcat/webapps/ROOT/WEB-INF/modules
RUN /bin/bash -c 'cd /usr/local/tomcat/webapps/ROOT/WEB-INF/modules/; \
for FILE in */; do cd $FILE; zip -q -r "../${PWD##*/}.zip" *; cd ..; rm -rf $FILE; done'
HEALTHCHECK --interval=30s --timeout=1s --start-period=300s CMD curl -sf http://localhost:8080/ping || exit 1