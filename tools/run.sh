#!/bin/bash

# force working dir to project's root
cd "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/.."

docker build -t demosimplicite ./ #--pull
docker run -it --platform linux/amd64 --rm --name demosimplicite -p 80:8080 demosimplicite