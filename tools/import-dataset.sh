#!/bin/sh

INSTANCE=$1
USER=$2
PASSWORD=$3
CREDS="$USER:$PASSWORD"

echo "Check app health for $INSTANCE"
curl -sm 1 $INSTANCE/ping
echo ""

echo "Import Demo dataset"
AUTH_HEADER="Authorization: Bearer $(curl -s -u $CREDS $INSTANCE/api/login?_output=token | jq -r .authtoken)"
DATASET_ROWID=$(curl -s -H "$AUTH_HEADER" -X GET "$INSTANCE/api/rest/Dataset?row_module_id__mdl_name=Demo" -H "accept: application/json" | jq .[0].row_id | sed "s/\"//g")

curl -s -H "$AUTH_HEADER" -X GET "$INSTANCE/api/rest/Dataset/$DATASET_ROWID/action:Dataset-apply"