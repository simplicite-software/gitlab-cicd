#!/bin/sh

# force working dir to project's root
echo "---------------------------"
pwd
echo ${BASH_SOURCE[0]}
echo "$( dirname -- "${BASH_SOURCE[0]}" )"
cd "$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/.."
pwd
echo "---------------------------"

INSTANCE=$1
USER=$2
PASSWORD=$3
CREDS="$USER:$PASSWORD"

echo "Run unit tests via /io for $INSTANCE"
echo ""
pwd
ls -halt

cat simap.json | jq -r '.modules[] | .unittests[]?' | while read tst;
do
    curl -s -u $CREDS --form service=unittests --form test=$tst $INSTANCE/io
done

echo ""
echo "Done."
