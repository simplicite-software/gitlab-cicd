# Tools docs

- `export.sh` : triggered by the developer to download the modules in this repo
- `import-dataset.sh https://my.instance.com user password` : triggered by CI/CD to trigger dataset import before UI testing
- `run.sh` :  used by developer to run instance locally
- `unit-tests.sh https://my.instance.com user password` triggered by CI/CD to trigger unit testing